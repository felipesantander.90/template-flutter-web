import 'package:http/http.dart';

class Api {
  static Future<Response> login(user, password, reponseBody) async {
    String userFake = 'admin@admin.cl';
    String passFake = 'admin';
    if (user == userFake && password == passFake) {
      return Response(reponseBody, 200);
    } else {
      return Response('', 400);
    }
  }

  static Future<Response> response200(body) async {
    await Future.delayed(const Duration(seconds: 2));
    return Response(body, 200);
  }
}
