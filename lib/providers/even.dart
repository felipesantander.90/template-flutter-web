import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:template_flutter_web/api/event.dart';
import 'package:template_flutter_web/models/event.dart';

class EventProvider extends ChangeNotifier with EventService {
  List<EventModel> events = [];

  List<EventModel> makeListEvent(AsyncSnapshot snapshot) {
    List<EventModel> list = [];
    json.decode(utf8.decode(snapshot.data!.bodyBytes)).forEach(
        (sourceDataEvent) => list.add(EventModel.fromMap(sourceDataEvent)));
    events = list;
    return list;
  }
}
