import 'package:flutter/foundation.dart';
import 'package:http/http.dart';
import 'package:template_flutter_web/api/account.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template_flutter_web/models/user.dart';
import 'dart:convert';

class UserProvider extends ChangeNotifier with AccountService {
  UserModel? userRegistred;

  Future<UserModel> getSavedUser() async {
    final Future<SharedPreferences> oPrefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await oPrefs;
    UserModel savedUser = UserModel(
      token: prefs.getString('token') ?? '',
      userId: prefs.getInt('userId') ?? 0,
      email: prefs.getString('email') ?? '',
      remenberMe: prefs.getBool('remenberMe') ?? false,
    );
    return savedUser;
  }

  Future<bool> deleteSavedUser() async {
    final Future<SharedPreferences> oPrefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await oPrefs;
    prefs.remove('token');
    prefs.remove('userId');
    prefs.remove('email');
    prefs.remove('remenberMe');
    return true;
  }

  Future<bool> saveInfoUser(Response response,
      {bool remenberMe = false}) async {
    final Future<SharedPreferences> oPrefs = SharedPreferences.getInstance();
    final SharedPreferences prefs = await oPrefs;
    var jsonBody = json.decode(response.body);
    jsonBody['remenberMe'] = remenberMe;
    UserModel oLogin = UserModel.fromMap(jsonBody);
    await prefs.setString('token', oLogin.token);
    await prefs.setInt('userId', oLogin.userId);
    await prefs.setString('email', oLogin.email);
    await prefs.setBool('remenberMe', oLogin.remenberMe);
    return true;
  }

  Future<Response> checkLogin({
    String user = '',
    String password = '',
  }) async {
    final responseCheckUser = userRegistred!.token == ''
        ? await checkLoginFetch(user, password)
        : await checkTokenFetch(userRegistred!.token);
    return responseCheckUser;
  }
}
