import 'package:flutter/material.dart';
import 'package:template_flutter_web/components/buttonMenu/index.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:template_flutter_web/views/dashboard/index.dart';

class MenuProvider extends ChangeNotifier {
  String? titleMenuSelected = Esp.menuButton1;
  Widget? menuSelected = const Data();
  List menuItems = [
    {
      'title': Esp.menuButton1,
      'icon': Icons.home,
      'Widget': const Data(),
    },
    {
      'title': Esp.menuButton2,
      'icon': Icons.event,
      'Widget': const Text(Esp.menuButton2),
    },
    {
      'title': Esp.menuButton2,
      'icon': Icons.settings,
      'Widget': const Text(Esp.menuButton3),
    },
  ];
  void setMenuSelected(String title) {
    titleMenuSelected = title;
    menuSelected =
        menuItems.firstWhere((item) => item['title'] == title)['Widget'];
    notifyListeners();
  }

  List<Widget> createMenu() {
    return menuItems
        .map((item) => ButtonMenu(title: item['title'], icon: item['icon']))
        .toList();
  }
}
