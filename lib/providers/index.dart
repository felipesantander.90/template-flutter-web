import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:template_flutter_web/providers/device.dart';
import 'package:template_flutter_web/providers/even.dart';
import 'package:template_flutter_web/providers/munu.dart';
import 'package:template_flutter_web/providers/user.dart';

List<SingleChildWidget> indexProvider = [
  ChangeNotifierProvider<UserProvider>(
    create: (context) => UserProvider(),
  ),
  ChangeNotifierProvider<MenuProvider>(
    create: (context) => MenuProvider(),
  ),
  ChangeNotifierProvider<DeviceProvider>(
    create: (context) => DeviceProvider(),
  ),
  ChangeNotifierProvider<EventProvider>(
    create: (context) => EventProvider(),
  ),
];
