import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:template_flutter_web/api/device.dart';
import 'package:template_flutter_web/models/device.dart';

class DeviceProvider extends ChangeNotifier with DeviceService {
  DeviceModel? deviceSelected;
  List<DeviceModel> devices = [];
  List<DeviceModel> devicesSelected = [];

  Future<Response> index() {
    return indexFetch();
  }

  Future<Response> getDeviceData() {
    return getDeviceDataFetch(deviceSelected!.serial);
  }

  void searchDevice(String search, DeviceModel? deviceSelected) {
    List<DeviceModel> list = [];
    for (var device in devices) {
      if (device.name.toLowerCase().contains(search.toLowerCase())) {
        list.add(device);
      }
    }
    if (!list.contains(deviceSelected)) {
      list.insert(0, deviceSelected!);
    }
    devicesSelected = list;
    notifyListeners();
  }

  void setDevice(DeviceModel device) {
    deviceSelected = device;
    notifyListeners();
  }

  List<DeviceModel> makeListDevice(AsyncSnapshot snapshot) {
    List<DeviceModel> list = [];
    json.decode(utf8.decode(snapshot.data!.bodyBytes)).forEach(
        (sourceDataDevice) => list.add(DeviceModel.fromMap(sourceDataDevice)));
    devices = list;
    return list;
  }
}
