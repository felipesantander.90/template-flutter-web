import 'dart:ui';

import 'package:flutter/material.dart';

const Color backGroundColor = Color(0xFFf2f0ed);
const Color firstColor = Color(0xff008bcf);
const Color secondColor = Color(0xFF7b868a);
const Color thirdColor = Color(0xFFfafafa);
const Color fontColor1 = Color(0xFF303030);

// a gradiont decoration color
gradientDecoration(context) => BoxDecoration(
      gradient: LinearGradient(
        stops: const [.1],
        begin: Alignment.center,
        end: Alignment.center,
        tileMode: TileMode.mirror,
        colors: [Theme.of(context).backgroundColor],
      ),
    );

final lightTheme = ThemeData(
  fontFamily: 'Lato',
  inputDecorationTheme: const InputDecorationTheme(
    contentPadding: EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
    hintStyle: TextStyle(
        fontFeatures: [FontFeature.proportionalFigures()],
        fontSize: 15.0,
        color: fontColor1),
    focusedBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: firstColor, width: 2.0),
    ),
    enabledBorder: UnderlineInputBorder(
      borderSide: BorderSide(color: fontColor1, width: 2.0),
    ),
  ),
  elevatedButtonTheme: ElevatedButtonThemeData(
      style: ButtonStyle(
          elevation: MaterialStateProperty.all<double>(5.0),
          padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
              const EdgeInsets.symmetric(vertical: 14.0)),
          foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
          backgroundColor: MaterialStateProperty.all<Color>(firstColor),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
              RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12.0),
                  side: const BorderSide(color: thirdColor))))),
  primaryColor: firstColor,
  backgroundColor: backGroundColor,
  textTheme: const TextTheme(
    headline1: TextStyle(color: fontColor1),
    headline2: TextStyle(color: fontColor1),
    headline3: TextStyle(color: fontColor1),
    headline4: TextStyle(color: fontColor1),
    headline5: TextStyle(color: fontColor1),
    headline6: TextStyle(color: fontColor1),
    bodyText1: TextStyle(color: fontColor1),
    bodyText2: TextStyle(color: fontColor1),
    subtitle1: TextStyle(color: fontColor1),
  ),
);
