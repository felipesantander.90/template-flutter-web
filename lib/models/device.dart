// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class DeviceModel {
  String serial;
  String name;
  String type;
  DeviceModel({
    required this.serial,
    required this.name,
    required this.type,
  });

  DeviceModel copyWith({
    String? serial,
    String? name,
    String? type,
  }) {
    return DeviceModel(
      serial: serial ?? this.serial,
      name: name ?? this.name,
      type: type ?? this.type,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'serial': serial,
      'name': name,
      'type': type,
    };
  }

  factory DeviceModel.fromMap(Map<String, dynamic> map) {
    return DeviceModel(
      serial: map['serial'] as String,
      name: map['name'] as String,
      type: map['type'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory DeviceModel.fromJson(String source) =>
      DeviceModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'DeviceModel(serial: $serial, name: $name, type: $type)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is DeviceModel &&
        other.serial == serial &&
        other.name == name &&
        other.type == type;
  }

  @override
  int get hashCode => serial.hashCode ^ name.hashCode ^ type.hashCode;
}
