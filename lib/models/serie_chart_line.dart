// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class SerieChartLine {
  String name;
  String data;
  SerieChartLine({
    required this.name,
    required this.data,
  });

  SerieChartLine copyWith({
    String? name,
    String? data,
  }) {
    return SerieChartLine(
      name: name ?? this.name,
      data: data ?? this.data,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'name': name,
      'data': data,
    };
  }

  factory SerieChartLine.fromMap(Map<String, dynamic> map) {
    return SerieChartLine(
      name: map['name_series'] as String,
      data: "${map['data']}",
    );
  }

  String toJson() => json.encode(toMap());

  factory SerieChartLine.fromJson(String source) =>
      SerieChartLine.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() => 'SerieChartLine(name: $name, data: $data)';

  @override
  bool operator ==(covariant SerieChartLine other) {
    if (identical(this, other)) return true;

    return other.name == name && other.data == data;
  }

  @override
  int get hashCode => name.hashCode ^ data.hashCode;
}
