// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class UserModel {
  String token;
  int userId;
  String email;
  bool remenberMe;
  UserModel({
    required this.token,
    required this.userId,
    required this.email,
    required this.remenberMe,
  });

  UserModel copyWith({
    String? token,
    int? userId,
    String? email,
    bool? remenberMe,
  }) {
    return UserModel(
      token: token ?? this.token,
      userId: userId ?? this.userId,
      email: email ?? this.email,
      remenberMe: remenberMe ?? this.remenberMe,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'token': token,
      'userId': userId,
      'email': email,
      'remenberMe': remenberMe,
    };
  }

  factory UserModel.fromMap(Map<String, dynamic> map) {
    return UserModel(
      token: map['token'] as String,
      userId: map['user_id'] as int,
      email: map['email'] as String,
      remenberMe: map['remenberMe'] as bool,
    );
  }

  String toJson() => json.encode(toMap());

  factory UserModel.fromJson(String source) =>
      UserModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'UserModel(token: $token, userId: $userId, email: $email, remenberMe: $remenberMe)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is UserModel &&
        other.token == token &&
        other.userId == userId &&
        other.email == email &&
        other.remenberMe == remenberMe;
  }

  @override
  int get hashCode {
    return token.hashCode ^
        userId.hashCode ^
        email.hashCode ^
        remenberMe.hashCode;
  }
}
