// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'dart:convert';

class EventModel {
  int idEvent;
  String title;
  String description;
  String date;
  int state;
  String serialMachine;
  String nameMachine;
  EventModel({
    required this.idEvent,
    required this.title,
    required this.description,
    required this.date,
    required this.state,
    required this.serialMachine,
    required this.nameMachine,
  });

  EventModel copyWith({
    int? idEvent,
    String? title,
    String? description,
    String? date,
    int? state,
    String? serialMachine,
    String? nameMachine,
  }) {
    return EventModel(
      idEvent: idEvent ?? this.idEvent,
      title: title ?? this.title,
      description: description ?? this.description,
      date: date ?? this.date,
      state: state ?? this.state,
      serialMachine: serialMachine ?? this.serialMachine,
      nameMachine: nameMachine ?? this.nameMachine,
    );
  }

  Map<String, dynamic> toMap() {
    return <String, dynamic>{
      'idEvent': idEvent,
      'title': title,
      'description': description,
      'date': date,
      'state': state,
      'serialMachine': serialMachine,
      'nameMachine': nameMachine,
    };
  }

  factory EventModel.fromMap(Map<String, dynamic> map) {
    print(map);
    return EventModel(
      idEvent: map['id_event'] as int,
      title: map['title'] as String,
      description: map['description'] as String,
      date: map['date'] as String,
      state: map['state'] as int,
      serialMachine: map['serial_machine'] as String,
      nameMachine: map['name_machine'] as String,
    );
  }

  String toJson() => json.encode(toMap());

  factory EventModel.fromJson(String source) =>
      EventModel.fromMap(json.decode(source) as Map<String, dynamic>);

  @override
  String toString() {
    return 'EventModel(idEvent: $idEvent, title: $title, description: $description, date: $date, state: $state, serialMachine: $serialMachine, nameMachine: $nameMachine)';
  }

  @override
  bool operator ==(covariant EventModel other) {
    if (identical(this, other)) return true;

    return other.idEvent == idEvent &&
        other.title == title &&
        other.description == description &&
        other.date == date &&
        other.state == state &&
        other.serialMachine == serialMachine &&
        other.nameMachine == nameMachine;
  }

  @override
  int get hashCode {
    return idEvent.hashCode ^
        title.hashCode ^
        description.hashCode ^
        date.hashCode ^
        state.hashCode ^
        serialMachine.hashCode ^
        nameMachine.hashCode;
  }
}
