import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_flutter_web/models/event.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class Body extends StatefulWidget {
  final EventModel? event;
  const Body({Key? key, required this.event}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  bool onOver = false;
  Map<int, Color> eventColorState = {
    0: Colors.indigoAccent,
    1: Colors.amber,
    2: Colors.teal,
    3: Colors.redAccent,
  };
  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (event) {
        setState(() {
          onOver = true;
        });
      },
      onExit: (event) {
        setState(() {
          onOver = false;
        });
      },
      child: Padding(
          padding: const EdgeInsets.all(12),
          child: Container(
            padding: const EdgeInsets.all(2),
            decoration: BoxDecoration(
                color: onOver
                    ? eventColorState[widget.event!.state]
                    : firstColor.withAlpha(200),
                borderRadius: BorderRadius.circular(12)),
            child: AnimatedContainer(
              duration: const Duration(milliseconds: 400),
              padding: const EdgeInsets.symmetric(horizontal: 10),
              decoration: BoxDecoration(
                color: firstColor,
                borderRadius: BorderRadius.circular(12),
              ),
              height: onOver ? 70.0 : 50.0,
              child: Row(
                children: [
                  const Icon(
                    LineIcons.envelope,
                    color: thirdColor,
                  ),
                  const SizedBox(width: 10),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(widget.event!.title,
                          style: Theme.of(context)
                              .textTheme
                              .bodyMedium!
                              .copyWith(color: thirdColor)),
                      onOver
                          ? SizedBox(
                              width: 200,
                              child: Text(widget.event!.description,
                                  overflow: TextOverflow.ellipsis,
                                  softWrap: true,
                                  maxLines: 3,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodySmall!
                                      .copyWith(color: thirdColor)))
                          : const SizedBox(),
                    ],
                  )
                ],
              ),
            ),
          )),
    );
  }
}
