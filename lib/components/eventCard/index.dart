import 'package:animator/animator.dart';
import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_flutter_web/models/event.dart';
import 'package:template_flutter_web/components/eventCard/body.dart';
import 'package:widget_and_text_animator/widget_and_text_animator.dart';

import '../../themes/light_theme.dart';

class EventCard extends StatefulWidget {
  final EventModel event;
  const EventCard({Key? key, required this.event}) : super(key: key);

  @override
  State<EventCard> createState() => _EventCardState();
}

class _EventCardState extends State<EventCard> {
  bool onOver = false;

  @override
  Widget build(BuildContext context) {
    return Animator<Offset>(
      tween: Tween<Offset>(begin: const Offset(1, 0), end: const Offset(0, 0)),
      duration: const Duration(milliseconds: 400),
      cycles: 1,
      builder: (_, animatorState, __) => SlideTransition(
        position: animatorState.animation,
        child: Body(event: widget.event),
      ),
    );
  }
}
