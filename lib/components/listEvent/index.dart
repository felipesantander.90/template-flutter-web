import 'package:flutter/material.dart';
import 'package:template_flutter_web/components/eventCard/index.dart';
import 'package:template_flutter_web/models/event.dart';

class ListEvent extends StatefulWidget {
  final List<EventModel> events;
  const ListEvent({Key? key, required this.events}) : super(key: key);

  @override
  State<ListEvent> createState() => _ListEventState();
}

class _ListEventState extends State<ListEvent> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Wrap(
          spacing: 20,
          children: widget.events.map((event) {
            return EventCard(
              event: event,
            );
          }).toList(),
        ));
  }
}
