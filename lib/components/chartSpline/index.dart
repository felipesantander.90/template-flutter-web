import 'package:flutter/material.dart';
import 'package:high_chart/high_chart.dart';
import 'package:template_flutter_web/models/serie_chart_line.dart';

class ChartSpline extends StatefulWidget {
  final List<SerieChartLine> data;
  const ChartSpline({Key? key, required this.data}) : super(key: key);

  @override
  State<ChartSpline> createState() => _ChartSplineState();
}

String serie(name, data) => """
  {
      type: 'spline',
      name: '$name',
      data: $data,
      marker: {
          lineWidth: 2,
          lineColor: Highcharts.getOptions().colors[3],
          fillColor: 'white'
      }
  }
""";

String makeJson(List<SerieChartLine> data) {
  List<String> list = [];
  for (SerieChartLine _serie in data) {
    list.add(serie(_serie.name, _serie.data));
  }
  return list.join(',');
}

String _chartData(series) => '''{
      chart: {
        backgroundColor: 'transparent',
      },
      title: {
          text: 'data chart',
      },   
      series: [${makeJson(series)}],
    }''';

class _ChartSplineState extends State<ChartSpline> {
  @override
  Widget build(BuildContext context) {
    String chartData = _chartData(widget.data);
    return Expanded(
        flex: 1,
        child: HighCharts(
          loader: const SizedBox(
            child: LinearProgressIndicator(),
          ),
          size: Size(
            MediaQuery.of(context).size.width,
            MediaQuery.of(context).size.height,
          ),
          data: chartData,
          scripts: const [
            "https://code.highcharts.com/highcharts.js",
            'https://code.highcharts.com/modules/networkgraph.js',
            'https://code.highcharts.com/modules/exporting.js',
          ],
        ));
  }
}
