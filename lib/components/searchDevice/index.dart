import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/providers/device.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

import '../../strings/index.dart';

class SeachDevice extends StatefulWidget {
  const SeachDevice({Key? key}) : super(key: key);

  @override
  State<SeachDevice> createState() => _SeachDeviceState();
}

class _SeachDeviceState extends State<SeachDevice> {
  @override
  Widget build(BuildContext context) {
    DeviceProvider deviceProvider = Provider.of<DeviceProvider>(context);
    DeviceProvider deviceSelectdProvider = Provider.of<DeviceProvider>(context);
    return Flexible(
        child: TextField(
      onChanged: (msg) {
        deviceProvider.searchDevice(msg, deviceSelectdProvider.deviceSelected);
      },
      decoration: const InputDecoration(
        contentPadding: EdgeInsets.symmetric(horizontal: 8.0),
        border: OutlineInputBorder(),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: secondColor,
            width: 1,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(
            color: firstColor,
            width: 1,
          ),
        ),
        labelText: Esp.searchHint,
      ),
    ));
  }
}
