import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:template_flutter_web/providers/munu.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class ButtonMenu extends StatefulWidget {
  final String title;
  final IconData icon;
  const ButtonMenu({Key? key, required this.title, required this.icon})
      : super(key: key);

  @override
  State<ButtonMenu> createState() => _ButtonMenuState();
}

class _ButtonMenuState extends State<ButtonMenu> {
  Color colorBackground = Colors.white.withOpacity(.2);
  Color colorText = thirdColor;
  @override
  Widget build(BuildContext context) {
    final mediaQueryData = MediaQuery.of(context);
    final width = mediaQueryData.size.width;
    MenuProvider menuProvider =
        Provider.of<MenuProvider>(context, listen: true);
    return MouseRegion(
        onEnter: (event) {
          setState(() {
            colorBackground = backGroundColor;
            colorText = fontColor1;
          });
        },
        onExit: (event) {
          setState(() {
            colorBackground = Colors.white.withOpacity(.2);
            colorText = thirdColor;
          });
        },
        child: GestureDetector(
            onTap: (() {
              menuProvider.setMenuSelected(widget.title);
            }),
            child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 2, vertical: 8),
                margin: const EdgeInsets.only(bottom: 8),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(12),
                  color: menuProvider.titleMenuSelected == widget.title
                      ? backGroundColor
                      : colorBackground,
                ),
                child: ResponsiveGridRow(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      ResponsiveGridCol(
                          xs: 12,
                          md: 3,
                          lg: 3,
                          xl: 3,
                          child: Icon(
                            widget.icon,
                            color:
                                menuProvider.titleMenuSelected == widget.title
                                    ? fontColor1
                                    : colorText,
                          )),
                      (width >= 700)
                          ? ResponsiveGridCol(
                              xs: 0,
                              md: 9,
                              lg: 9,
                              xl: 9,
                              child: Text(widget.title,
                                  softWrap: false,
                                  style: Theme.of(context)
                                      .textTheme
                                      .bodyText1!
                                      .copyWith(
                                          color:
                                              menuProvider.titleMenuSelected ==
                                                      widget.title
                                                  ? fontColor1
                                                  : colorText)))
                          : ResponsiveGridCol(
                              child: Container(),
                            ),
                    ]))));
  }
}
