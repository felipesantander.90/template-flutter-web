import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/components/listDevice/item_list_device.dart';
import 'package:template_flutter_web/models/device.dart';
import 'package:template_flutter_web/providers/device.dart';

class ListDevice extends StatefulWidget {
  final List<DeviceModel>? data;
  const ListDevice(this.data, {Key? key}) : super(key: key);

  @override
  State<ListDevice> createState() => _ListDeviceState();
}

class _ListDeviceState extends State<ListDevice> {
  late DeviceProvider deviceSelectdProvider;

  @override
  void initState() {
    super.initState();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      deviceSelectdProvider =
          Provider.of<DeviceProvider>(context, listen: false);
      if (deviceSelectdProvider.deviceSelected == null) {
        deviceSelectdProvider.setDevice(widget.data!.first);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return makeListDeviceView(widget.data!);
  }
}

Widget makeListDeviceView(List<DeviceModel> listDevice) {
  return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Wrap(
        spacing: 20,
        children: listDevice.map((device) {
          return ItemListDevice(
            device: device,
          );
        }).toList(),
      ));
}
