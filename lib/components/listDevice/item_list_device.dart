import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/models/device.dart';
import 'package:template_flutter_web/providers/device.dart';

import '../../themes/light_theme.dart';

class ItemListDevice extends StatefulWidget {
  final DeviceModel device;
  const ItemListDevice({Key? key, required this.device}) : super(key: key);

  @override
  State<ItemListDevice> createState() => _ItemListDeviceState();
}

class _ItemListDeviceState extends State<ItemListDevice> {
  Color colorBackground = firstColor;
  Color colorText = thirdColor;
  @override
  Widget build(BuildContext context) {
    DeviceProvider deviceSelectdProvider = Provider.of<DeviceProvider>(context);
    return GestureDetector(
      onTap: () {
        deviceSelectdProvider.setDevice(widget.device);
      },
      child: MouseRegion(
          onEnter: (event) {
            setState(() {
              colorBackground = backGroundColor;
              colorText = fontColor1;
            });
          },
          onExit: (event) {
            setState(() {
              colorBackground = firstColor;
              colorText = thirdColor;
            });
          },
          child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 1, vertical: 1),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: firstColor,
              ),
              child: Container(
                  padding: const EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    color: deviceSelectdProvider.deviceSelected == widget.device
                        ? backGroundColor
                        : colorBackground,
                  ),
                  height: 100,
                  width: 120,
                  child: Center(
                      child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        LineIcons.raspberryPi,
                        color: deviceSelectdProvider.deviceSelected ==
                                widget.device
                            ? fontColor1
                            : colorText,
                      ),
                      Text(widget.device.name,
                          maxLines: 1,
                          softWrap: false,
                          style: Theme.of(context)
                              .textTheme
                              .headline6!
                              .copyWith(
                                overflow: TextOverflow.fade,
                                color: deviceSelectdProvider.deviceSelected ==
                                        widget.device
                                    ? fontColor1
                                    : colorText,
                              )),
                    ],
                  ))))),
    );
  }
}
