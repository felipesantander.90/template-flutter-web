import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:template_flutter_web/strings/index.dart';

import '../views/errors/re_intent.dart';

Widget? snapshotValidation(
    AsyncSnapshot<Response> snapshot, String direction, Widget loadingWidget) {
  if (snapshot.connectionState == ConnectionState.waiting) {
    return loadingWidget;
  }
  if (snapshot.hasError) {
    return ReIntent(title: Esp.reIntent, direction: direction);
  }
  if (snapshot.connectionState == ConnectionState.done) {
    if (snapshot.data?.statusCode == 200) {
      return null;
    } else {
      return ReIntent(title: Esp.reIntent, direction: direction);
    }
  }
  return ReIntent(title: Esp.reIntent, direction: direction);
}
