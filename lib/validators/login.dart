String? passwordValidate(password) {
  if (password!.isEmpty) {
    return 'Ingrese Contraseña';
  }
  return null;
}

// ignore: top_level_function_literal_block
String? emailValidate(mail) {
  if (mail!.isEmpty) {
    return 'Ingrese email.';
  }
  if (isValidEmail(mail) == false) {
    return 'Email no valido';
  }
  return null;
}

bool isValidEmail(mail) {
  return RegExp(
          r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
      .hasMatch(mail);
}
