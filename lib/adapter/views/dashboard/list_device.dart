import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:skeletons/skeletons.dart';
import 'package:template_flutter_web/components/listDevice/index.dart';

import '../../../providers/device.dart';
import '../../../routers/router.dart';
import '../../../validators/api_response.dart';

class ListDeviceAdapter extends StatefulWidget {
  const ListDeviceAdapter({Key? key}) : super(key: key);

  @override
  State<ListDeviceAdapter> createState() => _ListDeviceAdapterState();
}

class _ListDeviceAdapterState extends State<ListDeviceAdapter> {
  @override
  Widget build(BuildContext context) {
    DeviceProvider deviceProvider = Provider.of<DeviceProvider>(context);
    if (deviceProvider.devicesSelected.isNotEmpty) {
      return ListDevice(deviceProvider.devicesSelected);
    }
    if (deviceProvider.devices.isNotEmpty) {
      return ListDevice(deviceProvider.devices);
    }
    return FutureBuilder(
      future: deviceProvider.index(),
      builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
        Widget? snapValidated = snapshotValidation(
            snapshot, PathRouter.dashboard, loadingWidget(context));
        if (snapValidated != null) {
          return snapValidated;
        } else {
          return ListDevice(deviceProvider.makeListDevice(snapshot));
        }
      },
    );
  }
}

SkeletonAvatar square(context) => SkeletonAvatar(
    style: SkeletonAvatarStyle(
        width: MediaQuery.of(context).size.width / 9, height: 100));

Row Function(dynamic context) loadingWidget = (context) => Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        square(context),
        square(context),
        square(context),
        square(context),
        square(context),
      ],
    );
