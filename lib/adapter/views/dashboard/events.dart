import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:skeletons/skeletons.dart';
import 'package:template_flutter_web/components/eventCard/index.dart';
import 'package:template_flutter_web/components/listEvent/index.dart';
import 'package:template_flutter_web/models/event.dart';
import 'package:template_flutter_web/providers/even.dart';
import 'package:template_flutter_web/routers/router.dart';

import '../../../validators/api_response.dart';

class EventAdapter extends StatefulWidget {
  const EventAdapter({Key? key}) : super(key: key);

  @override
  State<EventAdapter> createState() => _EventAdapterState();
}

class _EventAdapterState extends State<EventAdapter> {
  @override
  Widget build(BuildContext context) {
    EventProvider eventProvider = Provider.of<EventProvider>(context);
    return FutureBuilder(
      future: eventProvider.indexFetch(),
      builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
        Widget? snapValidated = snapshotValidation(
            snapshot, PathRouter.dashboard, loadingWidget(context));
        if (snapValidated != null) {
          return snapValidated;
        } else {
          List<EventModel> events = eventProvider.makeListEvent(snapshot);
          return ListEvent(
            events: events,
          );
        }
      },
    );
  }
}

SkeletonAvatar square(context) => SkeletonAvatar(
    style: SkeletonAvatarStyle(
        width: MediaQuery.of(context).size.width, height: 60));

Widget Function(dynamic context) loadingWidget = (context) => Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20),
    child: SingleChildScrollView(
      child: Wrap(
        spacing: 20, // to apply margin in the main axis of the wrap
        runSpacing: 20,
        children: [
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
          square(context),
        ],
      ),
    ));
