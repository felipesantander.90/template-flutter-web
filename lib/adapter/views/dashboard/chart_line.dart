import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:skeletons/skeletons.dart';
import 'package:template_flutter_web/components/chartSpline/index.dart';
import 'package:template_flutter_web/providers/device.dart';
import 'package:template_flutter_web/routers/router.dart';
import 'package:template_flutter_web/validators/api_response.dart';
import 'dart:convert';

import '../../../models/serie_chart_line.dart';

class ChartLineAdapter extends StatefulWidget {
  const ChartLineAdapter({Key? key}) : super(key: key);

  @override
  State<ChartLineAdapter> createState() => _ChartLineAdapterState();
}

class _ChartLineAdapterState extends State<ChartLineAdapter> {
  @override
  Widget build(BuildContext context) {
    DeviceProvider deviceProvider = Provider.of<DeviceProvider>(context);
    return FutureBuilder(
      future: deviceProvider.getDeviceData(),
      builder: (BuildContext context, AsyncSnapshot<Response> snapshot) {
        Widget? snapValidated = snapshotValidation(
            snapshot, PathRouter.dashboard, loadingWidget(context));
        if (snapValidated != null) {
          return snapValidated;
        } else {
          return ChartSpline(
            data: makeListSerieChartLine(snapshot),
          );
        }
      },
    );
  }
}

Widget loadingWidget(context) => SkeletonAvatar(
        style: SkeletonAvatarStyle(
      width: double.infinity,
      height: MediaQuery.of(context).size.height,
      minHeight: MediaQuery.of(context).size.height / 3,
      maxHeight: MediaQuery.of(context).size.height / 2,
    ));

List<SerieChartLine> makeListSerieChartLine(AsyncSnapshot snapshot) {
  List<SerieChartLine> list = [];
  dynamic data = json.decode(utf8.decode(snapshot.data!.bodyBytes))['series'];
  data.forEach(
      (sourceDataDevice) => list.add(SerieChartLine.fromMap(sourceDataDevice)));
  return list;
}
