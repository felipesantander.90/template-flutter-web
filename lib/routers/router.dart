import 'package:template_flutter_web/views/home/index.dart';
import 'package:template_flutter_web/views/login/index.dart';

class PathRouter {
  static const String login = '/';
  static const String dashboard = '/dashboard';
}

var routes = {
  PathRouter.login: (context) => const LoginView(),
  PathRouter.dashboard: (context) => const Dashboard(),
};
