class Errors {
  static const reIntent = 'Ocurrio un error Intente mas tarde';
  static const errorCheckLogin =
      "Algo salio mal. Usuario o Contraseña incorrectas";
}

class Esp {
  static const appTittle = 'WEBOX MONITOREO';
  static const tittleLoginRigth = 'Sistema de Monitoreo';
  static const subTittleLoginRigth = 'ecosistema';
  static const buttonLogin = 'INGRESAR';
  static const linkCompany =
      'https://gitlab.com/felipesantander.90/template-flutter-web';
  static const menuButton1 = 'Dashboard';
  static const menuButton2 = 'Evento';
  static const menuButton3 = 'Configuración';
  static const searchHint = 'Buscar Dispositivos';
  static const evenTitle = 'Eventos';
  static const reIntent = Errors.reIntent;
  static const errorCheckLogin = Errors.errorCheckLogin;
}

class Assets {
  static String companyIcon = 'lib/assets/common/companyIcon.png';
  static String loginWallpaper = 'lib/assets/login/loginWallpaper.webp';
  static String chip = 'lib/assets/common/chip.png';
  static String migration = 'lib/assets/common/migration.png';
}
