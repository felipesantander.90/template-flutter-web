import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template_flutter_web/dataFake/api.dart';
import 'package:template_flutter_web/dataFake/event.dart';

import '../dataFake/device.dart';

bool debug = dotenv.get('DEBUG').toLowerCase() == 'true';
final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

abstract class EventService {
  String backend = '';
  String epEventIndex = 'event/index';

  Future<http.Response> indexFetch() async {
    final SharedPreferences prefs = await _prefs;
    String? token = prefs.getString('token');
    final response = debug
        ? await Api.response200(EventFake.eventsNotification)
        : await http.get(Uri.https(backend, epEventIndex),
            headers: {'Authorization': 'Bearer $token'});
    return response;
  }
}
