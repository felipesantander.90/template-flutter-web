import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:crypto/crypto.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:template_flutter_web/dataFake/api.dart';
import 'package:template_flutter_web/dataFake/user.dart';

bool debug = dotenv.get('DEBUG').toLowerCase() == 'true';

abstract class AccountService {
  String backend = '';
  String epCheckLogin = 'auth-jwt/';
  String epCheckToken = 'auth-jwt/valid_token/';

  Future<http.Response> checkLoginFetch(String user, String password) async {
    final response = debug
        ? await Api.login(user, password, UserFake.responseUserFake)
        : await http.post(Uri.https(backend, epCheckLogin), body: {
            "username": user,
            "password": md5.convert(utf8.encode(password)).toString()
          });
    return response;
  }

  Future<http.Response> checkTokenFetch(String token) async {
    final response = debug
        ? http.Response(UserFake.responseUserFake, 200)
        : await http.post(Uri.https(backend, epCheckToken), body: {
            "token": token,
          });
    return response;
  }
}
