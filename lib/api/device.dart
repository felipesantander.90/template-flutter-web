import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:template_flutter_web/dataFake/api.dart';

import '../dataFake/device.dart';

bool debug = dotenv.get('DEBUG').toLowerCase() == 'true';
final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

abstract class DeviceService {
  String backend = '';
  String epDeviceIndex = 'device/index';
  String epDeviceGet(serial) => 'device/$serial';
  String epDeviceData(serial) => 'device/$serial/data/';

  Future<http.Response> indexFetch() async {
    final SharedPreferences prefs = await _prefs;
    String? token = prefs.getString('token');
    final response = debug
        ? await Api.response200(DeviceFake.responseDeviceIndexFake)
        : await http.get(Uri.https(backend, epDeviceIndex),
            headers: {'Authorization': 'Bearer $token'});
    return response;
  }

  Future<http.Response> getDeviceDataFetch(String serial) async {
    final SharedPreferences prefs = await _prefs;
    String? token = prefs.getString('token');
    final response = debug
        ? await Api.response200(DeviceFake.responseGetDeviceDate)
        : await http.get(Uri.https(backend, epDeviceGet(serial)),
            headers: {'Authorization': 'Bearer $token'});
    return response;
  }
}
