import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:template_flutter_web/themes/light_theme.dart';
import 'package:template_flutter_web/views/login/login_form.dart';

class Body extends StatelessWidget {
  const Body({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    return Scaffold(
        body: Container(
      decoration: gradientDecoration(context),
      child: ResponsiveGridRow(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ResponsiveGridCol(
                xs: 12, md: 12, child: formSide(screenSize, context)),
            // ResponsiveGridCol(
            //   sm: 1,
            //   xs: 1,
            //   md: 6,
            //   child: infoSide(screenSize, context),
            // )
          ]),
    ));
  }
}

formSide(screenSize, context) => Container(
      height: screenSize.height,
      alignment: const Alignment(0, 0),
      child: const Center(child: LoginForm()),
    );

infoSide(screenSize, context) => Container(
      decoration: BoxDecoration(
          image: DecorationImage(
        image: AssetImage(Assets.migration),
        repeat: ImageRepeat.noRepeat,
      )),
      height: screenSize.height,
      child: BackdropFilter(
        filter: ImageFilter.blur(sigmaX: 1, sigmaY: 1),
        child: Container(
            padding: const EdgeInsets.all(80),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Text(
                  Esp.tittleLoginRigth,
                  style: Theme.of(context).textTheme.headline2,
                ),
                const SizedBox(height: 10),
                Text(
                  Esp.subTittleLoginRigth,
                  style: Theme.of(context).textTheme.headline5,
                ),
              ],
            )),
      ),
    );
