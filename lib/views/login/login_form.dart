import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:gradient_loading_button/gradient_loading_button.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:template_flutter_web/themes/light_theme.dart';
import 'package:top_snackbar_flutter/custom_snack_bar.dart';
import 'package:top_snackbar_flutter/top_snack_bar.dart';
import 'package:template_flutter_web/routers/router.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../providers/user.dart';
import '../../validators/login.dart';

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  bool varUser = false;
  TextEditingController emailControler = TextEditingController();
  TextEditingController passwordControler = TextEditingController();
  final keyForm = GlobalKey<FormState>();

  late bool passwordVisible;

  @override
  void initState() {
    super.initState();
    passwordVisible = false;
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    setState(() {
      if (userProvider.userRegistred != null &&
          userProvider.userRegistred!.remenberMe) {
        emailControler.text = userProvider.userRegistred!.email;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final node = FocusScope.of(context);
    var userProvider = Provider.of<UserProvider>(context, listen: false);

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
        MaterialState.selected,
      };
      if (states.any(interactiveStates.contains)) {
        return Theme.of(context).primaryColor;
      }
      return Theme.of(context).primaryColor;
    }

    onSubmit(controller) async {
      // await _showNotification();
      if (keyForm.currentState!.validate()) {
        await controller.loading();
        final validLogin = await userProvider.checkLogin(
            user: emailControler.text, password: passwordControler.text);
        if (validLogin.statusCode == 200) {
          await controller.success();
          await userProvider.saveInfoUser(validLogin,
              remenberMe:
                  userProvider.userRegistred!.remenberMe ? true : varUser);
          await Future.delayed(const Duration(seconds: 1));
          Future.delayed(Duration.zero).then((_) {
            Navigator.pushNamed(context, PathRouter.dashboard);
          });
        } else {
          await controller.error();
          Future.delayed(Duration.zero).then((_) {
            showTopSnackBar(
                context,
                const CustomSnackBar.error(
                  message: Esp.errorCheckLogin,
                ));
          });
        }
      }
    }

    final loginButton = Container(
      margin: const EdgeInsets.only(top: 16.0),
      child: LoadingButton(
          errorChild: const Icon(
            Icons.close_sharp,
            color: Colors.white,
          ),
          successChild: const Icon(
            Icons.check_sharp,
            color: Colors.white,
          ),
          onPressed: onSubmit,
          child: const Text(
            Esp.buttonLogin,
          )),
    );

    final emailField = TextFormField(
      enabled: userProvider.userRegistred!.remenberMe ? false : true,
      onEditingComplete: () => node.nextFocus(),
      controller: emailControler,
      validator: emailValidate,
      style: Theme.of(context).textTheme.headline5,
      decoration: const InputDecoration(
        hintText: "Email",
      ),
    );

    final passwordField = TextFormField(
        onEditingComplete: () => node.nextFocus(),
        controller: passwordControler,
        validator: passwordValidate,
        obscureText: !passwordVisible,
        style: Theme.of(context).textTheme.headline5,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.fromLTRB(0, 15.0, 20.0, 15.0),
          hintText: 'Ingresa tu contraseña',
          // Here is key idea
          suffixIcon: IconButton(
            icon: Icon(
              passwordVisible ? Icons.visibility : Icons.visibility_off,
            ),
            onPressed: () {
              setState(() {
                passwordVisible = !passwordVisible;
              });
            },
          ),
        ));

    final checkboxRemenberUser = Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Checkbox(
          fillColor: MaterialStateProperty.resolveWith(getColor),
          onChanged: (bool? value) {
            setState(() {
              varUser = value!;
            });
          },
          value: varUser,
        ),
        Text('Recuerdame', style: Theme.of(context).textTheme.headline6)
      ],
    );

    final removeSavedUser = InkWell(
      onTap: () async {
        userProvider.deleteSavedUser();
        Navigator.pushNamed(context, '/');
      },
      child: Container(
        margin: const EdgeInsets.only(top: 12),
        child: const Text("Ingresar con otra cuenta",
            style: TextStyle(decoration: TextDecoration.underline)),
      ),
    );

    generateContentLogin() {
      if (userProvider.userRegistred!.remenberMe) {
        return <Widget>[
          emailField,
          loginButton,
          removeSavedUser,
        ];
      } else {
        return <Widget>[
          emailField,
          passwordField,
          loginButton,
          checkboxRemenberUser,
        ];
      }
    }

    return Form(
      key: keyForm,
      child: Padding(
        padding: const EdgeInsets.all(40.0),
        child: Column(
          verticalDirection: VerticalDirection.down,
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Image.asset(
              Assets.companyIcon,
            ),
            ResponsiveGridRow(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  ResponsiveGridCol(
                    xs: 1,
                    md: 3,
                    child: Container(),
                  ),
                  ResponsiveGridCol(
                      xs: 12,
                      md: 6,
                      child: Container(
                        height: 400,
                        padding: const EdgeInsets.symmetric(
                            vertical: 40, horizontal: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: thirdColor,
                        ),
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: generateContentLogin()),
                      )),
                ]),
            RichText(
                text: TextSpan(
              children: [
                TextSpan(
                  text: 'Powered by FSantander',
                  style: Theme.of(context)
                      .textTheme
                      .bodyText1!
                      .copyWith(decoration: TextDecoration.underline),
                  recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      launchUrl(Uri(host: Esp.linkCompany));
                    },
                ),
              ],
            )),
          ],
        ),
      ),
    );
  }
}
