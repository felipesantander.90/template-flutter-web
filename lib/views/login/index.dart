import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:template_flutter_web/components/circularCharger/index.dart';
import 'package:template_flutter_web/models/user.dart';
import 'package:template_flutter_web/providers/user.dart';
import 'package:template_flutter_web/views/login/body.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key? key}) : super(key: key);

  @override
  State<LoginView> createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  @override
  Widget build(BuildContext context) {
    UserProvider userProvider =
        Provider.of<UserProvider>(context, listen: false);
    return FutureBuilder(
      future: userProvider.getSavedUser(),
      builder: (BuildContext context, AsyncSnapshot<UserModel> snapshot) {
        if (snapshot.hasData) {
          userProvider.userRegistred = snapshot.data;
          return const Body();
        } else {
          return const CircularCharger();
        }
      },
    );
  }
}
