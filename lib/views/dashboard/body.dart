import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:skeletons/skeletons.dart';
import 'package:template_flutter_web/adapter/views/dashboard/chart_line.dart';
import 'package:template_flutter_web/providers/device.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class Body extends StatefulWidget {
  const Body({Key? key}) : super(key: key);

  @override
  State<Body> createState() => _BodyState();
}

class _BodyState extends State<Body> {
  @override
  Widget build(BuildContext context) {
    return Consumer<DeviceProvider>(
      builder: (context, deviceSelectdProvider, _) {
        return deviceSelectdProvider.deviceSelected != null
            ? Expanded(
                flex: 1,
                child: Container(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 1, vertical: 1),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(8),
                      color: firstColor,
                    ),
                    child: Container(
                      decoration: BoxDecoration(
                        color: backGroundColor,
                        borderRadius: BorderRadius.circular(8),
                      ),
                      padding: const EdgeInsets.all(20),
                      child: const ChartLineAdapter(),
                    )))
            : Expanded(
                flex: 1,
                child: SkeletonAvatar(
                    style: SkeletonAvatarStyle(
                  width: double.infinity,
                  height: MediaQuery.of(context).size.height,
                  minHeight: MediaQuery.of(context).size.height / 3,
                  maxHeight: MediaQuery.of(context).size.height / 2,
                )));
      },
    );
  }
}
