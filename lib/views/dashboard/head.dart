import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:template_flutter_web/adapter/views/dashboard/list_device.dart';
import 'package:template_flutter_web/components/searchDevice/index.dart';
import 'package:template_flutter_web/strings/index.dart';

class Head extends StatefulWidget {
  const Head({Key? key}) : super(key: key);

  @override
  State<Head> createState() => _HeadState();
}

class _HeadState extends State<Head> {
  @override
  Widget build(BuildContext context) {
    return ResponsiveGridRow(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          ResponsiveGridCol(
              xs: 12,
              md: 12,
              lg: 12,
              child: Text(
                Esp.menuButton1,
                style: Theme.of(context)
                    .textTheme
                    .headline5
                    ?.copyWith(decoration: TextDecoration.underline),
              )),
          ResponsiveGridCol(
              xs: 12,
              md: 12,
              lg: 12,
              child: const SizedBox(
                height: 32.0,
              )),
          ResponsiveGridCol(
            xs: 12,
            md: 4,
            lg: 4,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: const [
                SeachDevice(),
              ],
            ),
          ),
          ResponsiveGridCol(
              xs: 12,
              md: 12,
              lg: 12,
              child: const SizedBox(
                height: 16.0,
              )),
          ResponsiveGridCol(
            child: const ListDeviceAdapter(),
          )
        ]);
  }
}
