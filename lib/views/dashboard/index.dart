import 'package:flutter/material.dart';
import 'package:template_flutter_web/views/dashboard/head.dart';
import 'package:template_flutter_web/views/dashboard/body.dart';

class Data extends StatefulWidget {
  const Data({Key? key}) : super(key: key);

  @override
  State<Data> createState() => _DataState();
}

class _DataState extends State<Data> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.transparent,
        body: Padding(
            padding: const EdgeInsets.symmetric(vertical: 40, horizontal: 20),
            child: Column(
              children: const [Head(), SizedBox(height: 12), Body()],
            )));
  }
}
