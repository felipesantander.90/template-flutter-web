import 'package:flutter/material.dart';
import 'package:template_flutter_web/routers/router.dart';
import 'package:template_flutter_web/strings/index.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

GlobalKey globalKey = GlobalKey();

final GlobalKey<NavigatorState> navigatorKey =
    GlobalKey(debugLabel: "Main Navigator");
final index = MaterialApp(
  key: globalKey,
  navigatorKey: navigatorKey,
  title: Esp.appTittle,
  theme: lightTheme,
  initialRoute: PathRouter.login,
  routes: routes,
);
