import 'package:flutter/material.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:template_flutter_web/views/home/body.dart';
import 'package:template_flutter_web/views/home/left_side.dart';
import 'package:template_flutter_web/views/home/right_side.dart';

import '../../themes/light_theme.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: gradientDecoration(context),
      child: ResponsiveGridRow(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ResponsiveGridCol(xs: 2, md: 3, lg: 2, child: const RightSide()),
            ResponsiveGridCol(xs: 10, md: 8, lg: 8, child: const Body()),
            ResponsiveGridCol(xs: 1, md: 1, lg: 2, child: LeftSide()),
          ]),
    ));
  }
}
