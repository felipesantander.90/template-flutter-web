import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_grid/responsive_grid.dart';
import 'package:template_flutter_web/providers/munu.dart';

import '../../strings/index.dart';

class RightSide extends StatefulWidget {
  const RightSide({Key? key}) : super(key: key);

  @override
  State<RightSide> createState() => _RightSideState();
}

class _RightSideState extends State<RightSide> {
  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;
    MenuProvider menu = Provider.of<MenuProvider>(context, listen: false);
    return Container(
      padding: const EdgeInsets.only(left: 12, right: 12, top: 20),
      height: screenSize.height,
      color: Theme.of(context).primaryColor,
      child: ResponsiveGridRow(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ResponsiveGridCol(
                xs: 1,
                md: 12,
                lg: 12,
                child: Container(
                  padding: const EdgeInsets.all(24),
                  child: Image.asset(
                    Assets.companyIcon,
                    fit: BoxFit.contain,
                  ),
                )),
            ResponsiveGridCol(
                xs: 12,
                md: 12,
                lg: 12,
                child: Column(
                  children: menu.createMenu(),
                )),
          ]),
    );
  }
}
