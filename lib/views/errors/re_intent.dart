import 'package:flutter/material.dart';
import 'package:line_icons/line_icons.dart';
import 'package:template_flutter_web/themes/light_theme.dart';

class ReIntent extends StatelessWidget {
  final String title;
  final String direction;
  const ReIntent({Key? key, required this.title, required this.direction})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 60, vertical: 60),
      decoration: gradientDecoration(context),
      child:
          Column(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
        Image.asset(
          'lib/assets/titulo_login.png',
          fit: BoxFit.fitWidth,
          width: 500,
        ),
        Container(
          padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 60),
          decoration: BoxDecoration(
              color: secondColor.withOpacity(.3),
              borderRadius: const BorderRadius.all(Radius.circular(8))),
          child: Column(
            children: [
              const Icon(
                LineIcons.dragon,
                size: 40,
                color: Colors.red,
              ),
              const SizedBox(height: 20),
              Text(title,
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(fontSize: 24))
            ],
          ),
        ),
        ElevatedButton(
            style: ButtonStyle(
              padding: MaterialStateProperty.all<EdgeInsetsGeometry>(
                  const EdgeInsets.symmetric(vertical: 20.0, horizontal: 60)),
            ),
            onPressed: () {
              Navigator.pushNamed(context, direction);
            },
            child: const Text('Reintentar'))
      ]),
    );
  }
}
