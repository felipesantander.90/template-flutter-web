# Template Flutter Web V0.4

[FLUTTER] my personal template for start to work on new project flutter web

## Changelog V0.4
* new model event added
* all files for snake case type renamed
* new librery for animation added
* new loading for event added
* new animation for on an exit card event added

## images System

# Event Notification

![alt text](./images/last_change.gif)

# charger chart
![alt text](./images/carga_grafico.gif)
# login
![alt text](./images/login.png)
# Dashboard
![alt text](./images/dashboard.png)
# SearchBarDevice
![alt text](./images/searchbar.png)

## TO DO

* validation login from fake data ✅
* add loading for chart component ✅
* add responsibe for chart component ✅
* create main chart ✅
* add event to main view ✅
* add event view 
* config view

## Changelog V0.3
* laoding graph added.
* file name to snake case changed.
* first select item fixed
* data fake to chart added
* valid login now user/password is user: admin@admin.cl pwd:admin
* login error user added


## Changelog V0.2

* new panel added 
* new three blocks (side bar, main content, notification side) added,
* components, listItemDevice, Search Device, ButtonMenu added
* support for highChart added
* class for Esp String modificated, now the errors from for the other class error into same file
* new font Lote applied for lightTheme
* validator for response http added, for the views errors is needed change the view response

## Getting started

for this first stop i creted a template for login, this template have:

- multi provider
- custom theme
- view login created
- router generated
- folder String generated
- models user
- dataFoke for call api


## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/felipesantander.90/template-flutter-web.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/felipesantander.90/template-flutter-web/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
